function calculateIndividualIncome(income){
    let yearlyIncome=0;
    let yearlyTax=0;
    let monthlyTax=0;
    if(income<= 40000){
        yearlyTax = (1/100*income);
        yearlyIncome = income-yearlyTax;
        monthlyTax = yearlyTax/12;
    }else if(income<= 100000 && income>40000){
        yearlyTax = (10/100*income);
        yearlyIncome = income-yearlyTax;
        monthlyTax = yearlyTax/12;
    }else if(income<= 200000 && income>100000){
        yearlyTax = (20/100*income);
        yearlyIncome = income-yearlyTax;
        monthlyTax = yearlyTax/12;
    }else if(income<= 1300000 && income>200000){
        yearlyTax = (30/100*income);
        yearlyIncome = income-yearlyTax;
        monthlyTax = yearlyTax/12;
    }else if(income<= 3600000 && income>1300000){
        yearlyTax = (36/100*income);
        yearlyIncome = income-yearlyTax;
        monthlyTax = yearlyTax/12;
    }else {
        alert("Not found");
    }
    const resultel=document.getElementsByClassName("result");
    resultel[0].innerHTML="<div>Yearly income is" + yearlyIncome+"</div><br> <div>Yearly Tax is" + yearlyTax+"</div><br><div>Monthly Tax is" + monthlyTax+"</div>";

}
function calculateCoupleIncome(income){
    let yearlyIncome=0;
    let yearlyTax=0;
    let monthlyTax=0;
    if(income<= 45000){
        yearlyTax=(1/100*income);
        yearlyIncome = income-yearlyTax;
        monthlyTax = yearlyTax/12;
    }else if(income<= 100000 && income>45000){
        yearlyTax=(10/100*income);
        yearlyIncome = income-yearlyTax;
        monthlyTax = yearlyTax/12;
    }else if(income<= 200000 && income>100000){
        yearlyTax=(20/100*income);
        yearlyIncome = income-yearlyTax;
        monthlyTax = yearlyTax/12;
    }else if(income<= 1250000 && income>200000){
        yearlyTax=(30/100*income);
        yearlyIncome = income-yearlyTax;
        monthlyTax = yearlyTax/12;
    }else if(income<= 3600000 && income>1250000){
        yearlyTax=(36/100*income);
        yearlyIncome = income-yearlyTax;
        monthlyTax = yearlyTax/12;
    }else {
        alert("Not found");
    }
    const resultel=document.getElementsByClassName("result");
    resultel[0].innerHTML="<div>Yearly income is" + yearlyIncome+"</div><br> <div>Yearly Tax is" + yearlyTax+"</div><br><div>Monthly Tax is" + monthlyTax+"</div>";
}


function calculateIncome(){
    let status = document.getElementsByClassName("status")[0].value;
    let income = document.getElementsByClassName("income")[0].value;
    if (status === "couple"){
        calculateCoupleIncome(income);
    }else if (status === "individual"){
        calculateIndividualIncome(income);
    }else{
        alert("Marital Status not found.");
    }
}

