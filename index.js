function pyramidFront(num){
    for (i = 1; i <= num; i++) {
        for (j = 0; j < i; j++) {
            document.write(j + 1);
        }
        document.write("<br>");
    }
}

function pyramidBack(num) {
    for (i = num; i >= 0; i--) {
        for (j = 1; j <= i; j++) {
            document.write(j);
        }
        document.write("<br>");
    }
}


function showPyramid(){
    let value = document.getElementsByClassName("value")[0].value;
    let type = document.getElementsByClassName("type")[0].value;
    // if (value === ""){
    //         alert("String not allowed");
    //     }
    if (value && type) {
        if (type == 1) {
            pyramidFront(value);
        } else if (type == 2) {
            pyramidBack(value);
        }else{
            alert("not found");
        }
    }
}
